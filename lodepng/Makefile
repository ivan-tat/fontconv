#!/bin/make -f
#
# Supported environments:
#   * GNU on Linux, FreeBSD etc.
#   * GNU on Windows NT (using MinGW/MSYS/Cygwin/WSL)
#
# Build:
#   make [BUILD=<BUILD>]
# Install / Uninstall:
#   make [BUILD=<BUILD>] [prefix=<PREFIX>] install | uninstall
# Clean:
#   make [BUILD=<BUILD>] clean
#   make distclean
#
# where:
#   <BUILD> - see included `common.mak'.
#   <PREFIX> is a prefix directory to install files into.
#
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: Copyright (C) 2021-2024 Ivan Tatarinov
# SPDX-License-Identifier: GPL-3.0-or-later

include ../common.mak

srcdir		= .
builddir	?= build
prefix		?= /usr/local
exec_prefix	?= $(prefix)
libdir		?= $(exec_prefix)/lib

LIBS=\
 liblodepng$(DLLSUFFIX)

CFLAGS:=-ansi -pedantic -Wall -Wextra -O3 -Wno-unused-function $(CFLAGS)

.PHONY: all
all: build-libs

$(builddir) \
$(DESTDIR)$(libdir):
	mkdir -p $@

$(builddir)/liblodepng$(DLLSUFFIX):\
 $(srcdir)/lodepng.c\
 $(srcdir)/lodepng.h\
 Makefile | $(builddir)
	$(CC) -fPIC -shared $(CFLAGS) -o $@ $<

.PHONY: build-libs
build-libs: $(foreach f,$(LIBS),$(builddir)/$(f))

$(DESTDIR)$(libdir)/%$(DLLSUFFIX): $(builddir)/%$(DLLSUFFIX)\
 | $(DESTDIR)$(libdir)
	$(INSTALL_PROGRAM) -m 644 $< $@

.PHONY: install-libs
install-libs: $(foreach f,$(LIBS),$(DESTDIR)$(libdir)/$(f))

.PHONY: uninstall-libs
uninstall-libs:
	$(RM) $(foreach f,$(LIBS),$(DESTDIR)$(libdir)/$(f))

.PHONY: clean-libs
clean-libs:
	$(RM) $(foreach f,$(LIBS),$(builddir)/$(f))

.PHONY: distclean-libs
distclean-libs: clean-libs

.PHONY: install
install: install-libs

.PHONY: install-strip
install-strip:
	$(MAKE) -w INSTALL_PROGRAM='$(INSTALL_PROGRAM) -s' install

.PHONY: uninstall
uninstall: uninstall-libs

.PHONY: clean
clean: clean-libs

.PHONY: distclean
distclean: distclean-libs
	$(RM) -r $(builddir)
