/*
Description for LodePNG library.

SPDX-FileType: SOURCE
SPDX-FileCopyrightText: 2021-2024 Ivan Tatarinov
SPDX-License-Identifier: CC0-1.0
*/

#pragma once

#ifndef _LODEPNG_VER_H_INCLUDED
#define _LODEPNG_VER_H_INCLUDED 1

#define LIB_LODEPNG_NAME "LodePNG"
#define LIB_LODEPNG_VERSION "20230410"
#define LIB_LODEPNG_LICENSE "Zlib"
#define LIB_LODEPNG_COPYRIGHT "Copyright (c) 2005-2023 Lode Vandevenne"
#define LIB_LODEPNG_HOMEPAGE "https://github.com/lvandeve/lodepng"

#endif  /* !_LODEPNG_VER_H_INCLUDED */
