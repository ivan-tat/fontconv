#!/bin/make
#
# common.mak - common definitions for Makefiles
#
# Supported environments:
#   * GNU on Linux, FreeBSD etc.
#   * GNU on Windows NT (using MinGW/MSYS/Cygwin/WSL)
#
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: Copyright (C) 2021-2024 Ivan Tatarinov
# SPDX-License-Identifier: GPL-3.0-or-later

ifeq ($(BUILD),mingw32)
 CC = i686-w64-mingw32-gcc
else ifeq ($(BUILD),mingw64)
 CC = x86_64-w64-mingw32-gcc
else ifneq ($(BUILD),)
 $(error Unknown build: "$(BUILD)")
endif

# Check value of RELEASE
ifeq ($(RELEASE),linux)
else ifeq ($(RELEASE),windows)
else ifneq ($(RELEASE),)
 $(error Unknown release: "$(RELEASE)")
endif

ifeq ($(OS),Windows_NT)
 ifeq ($(RELEASE),linux)
  EXESUFFIX=
  DLLSUFFIX=.so
 else
  EXESUFFIX=.exe
  DLLSUFFIX=.dll
 endif
else ifeq ($(BUILD),mingw32)
 EXESUFFIX=.exe
 DLLSUFFIX=.dll
else ifeq ($(BUILD),mingw64)
 EXESUFFIX=.exe
 DLLSUFFIX=.dll
else ifeq ($(shell uname -s),Linux)
 ifeq ($(RELEASE),windows)
  EXESUFFIX=.exe
  DLLSUFFIX=.dll
 else
  EXESUFFIX=
  DLLSUFFIX=.so
 endif
else ifneq ($(DJGPP),)
 $(warn Not fully tested)
 EXESUFFIX=.exe
 DLLSUFFIX=
else
 $(error Unknown platform)
endif

INSTALL		?= install
INSTALL_PROGRAM	?= $(INSTALL)
INSTALL_DATA	?= $(INSTALL) -m 644
RM		?= rm -f
