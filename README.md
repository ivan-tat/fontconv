# The Right Tools

Tools for developers, mainly for ZX Spectrum platform. Available tools:
* **fontconv** - converter between an image and a binary font files
* **scrconv** - converter between an image, a binary bitmap, a binary ZX Spectrum's screen and a binary RCS files

# Authors

This document:
* Ivan Tatarinov, <ivan-tat at ya dot ru>, 2021-2024.

Main developer:
* Ivan Tatarinov, <ivan-tat at ya dot ru>, 2021-2024.

# Licenses

* This document: [GNU Free Documentation License v1.3 or later](LICENSES/GFDL-1.3-or-later.txt) ([SPDX link](https://spdx.org/licenses/GFDL-1.3-or-later.html))
* This software:
  * Source code: [GNU General Public License v3.0 or later](LICENSES/GPL-3.0-or-later.txt) ([SPDX link](https://spdx.org/licenses/GPL-3.0-or-later.html))
  * Small files: [Creative Commons Zero v1.0 Universal License](LICENSES/CC0-1.0.txt) ([SPDX link](https://spdx.org/licenses/CC0-1.0.html))
* LodePNG C/C++ library: [zlib License](LICENSES/Zlib.txt) ([SPDX link](https://spdx.org/licenses/Zlib.html))

I try to follow [REUSE recommendations](https://reuse.software/tutorial/) on how to easely specify copyright and licensing information to my files.
So I added this information to each source file I used according to [SPDX specification](https://spdx.dev/specifications/).
Check it out by using this [reuse-tool](https://github.com/fsfe/reuse-tool).

# Build and install software

Default values for parameters (variables):

Variable       | Default value
-------------- | ------------
`STATIC_BUILD` | `0`
`INCLUDEDIR`   | `lodepng`
`LIBDIR`       | `build`
`prefix`       | `/usr/local`

Change `INCLUDEDIR` and `LIBDIR` if you want to use your own LodePNG version.

You can save any redefinitions of these variables to the `conf.mk` file with standard Makefile syntax:

```Makefile
# Just an example:
export INCLUDEDIR = $(HOME)/devel/local/include
export LIBDIR := $(HOME)/devel/local/lib
export prefix ?= $(HOME)/devel/local
```

## Statically linked binaries

It can be helpful to save `STATIC_BUILD=1` parameter for `make` program permanently by typing this in a terminal:

```Shell
echo 'export STATIC_BUILD=1'>conf.mk
```

If you do this you can omit passing it to `make` program on every invocation (unbelievable!).

### Build

```Shell
make STATIC_BUILD=1 [INCLUDEDIR=PATH] [TARGET]
```

where `TARGET` if one of: `all`, `build-tools`

### Install

To install files in system directories, you must have permissions.

```Shell
make STATIC_BUILD=1 [prefix=PREFIX] TARGET
```

where `TARGET` is one of: `install-tools`, `install`, `install-strip`.

### Uninstall

To uninstall files from system directories, you must have permissions.

```Shell
make STATIC_BUILD=1 [prefix=PREFIX] TARGET
```

where `TARGET` is one of: `uninstall-tools`, `uninstall`.

### Clean

```Shell
make STATIC_BUILD=1 TARGET
```

where `TARGET` is one of: `clean-tools`, `clean`, `distclean-tools`, `distclean`.

## Dynamically linked library and binaries

### Build

#### Build only LodePNG library

```Shell
make STATIC_BUILD=0 build-lodepng
```

#### Build only tools

```Shell
make STATIC_BUILD=0 [INCLUDEDIR=<DIR>] [LIBDIR=<DIR>] build-tools
```

#### Build LodePNG library and tools

```Shell
make STATIC_BUILD=0 [all]
```

### Install

To install files in system directories, you must have permissions.

#### Install only LodePNG library

```Shell
make STATIC_BUILD=0 [prefix=PREFIX] install-lodepng
```

#### Install only tools

```Shell
make STATIC_BUILD=0 [prefix=PREFIX] install-tools
```

#### Install all

```Shell
make STATIC_BUILD=0 [prefix=PREFIX] install
```

### Uninstall

To uninstall files from system directories, you must have permissions.

#### Uninstall only LodePNG library

```Shell
make STATIC_BUILD=0 [prefix=PREFIX] uninstall-lodepng
```

#### Uninstall only tools

```Shell
make STATIC_BUILD=0 [prefix=PREFIX] uninstall-tools
```

#### Uninstall LodePNG library and tools

```Shell
make STATIC_BUILD=0 [prefix=PREFIX] uninstall
```

### Clean

#### Clean only LodePNG library

```Shell
make STATIC_BUILD=0 TARGET
```

where `TARGET` is one of: `clean-lodepng`, `distclean-lodepng`.

#### Clean only tools

```Shell
make STATIC_BUILD=0 TARGET
```

where `TARGET` is one of: `clean-tools`, `distclean-tools`.

#### Clean LodePNG library and tools

```Shell
make STATIC_BUILD=0 TARGET
```

where `TARGET` is one of: `clean`, `distclean`.

# Usage

## fontconv (font converter)

```
Usage:
  fontconv [options ...] input output

Options:
  -h      Show this help
  -q      Quiet output (default is verbose output)
  -d      Decode binary font file into an image (default is encode mode)
  -r      Rotate input characters for vertical output
  -f WxH  Input character size: W=width, H=height (default is 8x8)
  -t WxH  Output character size: W=width, H=height (defaults to "-f")
  -c N    Output N characters per line in output image (default is 16)

  input   Input PNG image file (or binary font file in "-d" mode)
  output  Output binary font file (or PNG image in "-d" mode)

Input image width must be a multiple of input character's width.
Input image height must be a multiple of input character's height.
Input and output color for background is pure black (i.e. R=G=B=0).
Input ink color is any other. Output ink color is pure white.
```

## scrconv (screen converter)

```
Usage:
  scrconv [options ...] [--] input output [attrs]

Options:
  -h       Show this help
  -q       Quiet output (default is verbose output)
  -s WxH   Image size: W=width, H=height (default is autodetect)
  -f type  Input file type (default is autodetect)
  -t type  Output file type (default depends on input type)
  -a file  Use this file with color attributes to process input image
  --       Stop parsing options, treat next arguments as filenames

  input    Input file
  output   Output file
  attrs    Output file with color attributes

Where "type" is:
  image, png   PNG image
  bitmap, bm   ZX Spectrum's bitmap data of any size and linear addressing
               (with or without color attributes)
  screen, scr  ZX Spectrum's screen 256x192 or it's part 256x64 or 256x128
               (with or without color attributes)
  rcs          ZX Spectrum's screen 256x192 or it's part 256x64 or 256x128
               (RCS address encoding with or without color attributes).

Default conversions (if only one option "-f" or "-t" given):
  -f png <--> -t scr
  -f bm    -> -t png
  -f scr <--> -t png
  -f rcs   -> -t png

Flashing bit in color attributes is not supported for output PNG image.
Width and height must be a multiple of 8.
It is possible for "bm" image to be of any size (use "-s" option to specify).
```

# References

* [REUSE SOFTWARE](https://reuse.software/) - a set of recommendations to make licensing your Free Software projects easier
* [The Software Package Data Exchange (SPDX)](https://spdx.dev/) - An open standard for communicating software bill of material information, including components, licenses, copyrights, and security references
* [GNU Operating System](https://www.gnu.org/)
* [GNU Standards](http://savannah.gnu.org/projects/gnustandards) - GNU coding and package maintenance standards ([package](https://pkgs.org/download/gnu-standards))
* [GNU Core Utilities](https://www.gnu.org/software/coreutils/) ([package](https://pkgs.org/download/coreutils))
* [GNU Bash](https://www.gnu.org/software/bash/) - GNU Bourne Again SHell ([package](https://pkgs.org/download/bash))
* [GNU Compiler Collection](https://www.gnu.org/software/gcc/) ([package](https://pkgs.org/download/gcc))
* [GNU Make](https://www.gnu.org/software/make/) - utility for directing compilation ([package](https://pkgs.org/download/make))
* [Cygwin](https://cygwin.com/) - a large collection of GNU and Open Source tools which provide functionality similar to a Linux distribution on Windows
* [MSYS2](https://www.msys2.org/) - a collection of tools and libraries providing you with an easy-to-use environment for building, installing and running native Windows software
* [MinGW](https://osdn.net/projects/mingw/) - Minimalist GNU for Windows
* [MinGW-w64](https://www.mingw-w64.org/) - an advancement of the original mingw.org project, created to support the GCC compiler on Windows systems
* [cmd](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/cmd) - command interpreter in Windows
* [Windows commands](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/windows-commands)
* [LodePNG](https://github.com/lvandeve/lodepng) - PNG encoder and decoder in C and C++
* [RCS (Reverse Computer Screen)](https://github.com/einar-saukas/RCS) - an utility to reorder bytes from ZX Spectrum screens before compression
