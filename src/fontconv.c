/*
 * fontconv - converter between a PNG image and a binary font file.
 *
 * Copyright (C) 2021-2024 Ivan Tatarinov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: Copyright (C) 2021-2024 Ivan Tatarinov
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdarg.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if STATIC_BUILD
# include "lodepng.c"
#else
# include "lodepng.h"
#endif
#include "lodepng_ver.h"

#define PROGRAM "fontconv"
#define DESCRIPTION "PNG image <=> binary font file converter."
#define VERSION "0.1.2"
#define COPYRIGHT "Copyright (C) 2021-2024 Ivan Tatarinov"
#define LICENSE                                                              \
"This program is free software: you can redistribute it and/or modify\n"     \
"it under the terms of the GNU General Public License as published by\n"     \
"the Free Software Foundation, either version 3 of the License, or\n"        \
"(at your option) any later version."
#define HOMEPAGE "https://gitlab.com/ivan-tat/therighttools"
#define THIRDPARTY                                                           \
"* " LIB_LODEPNG_NAME " library (compiled with version " LIB_LODEPNG_VERSION ")\n" \
"  License: " LIB_LODEPNG_LICENSE ". " LIB_LODEPNG_COPYRIGHT "\n"            \
"  Home page: <" LIB_LODEPNG_HOMEPAGE ">"

#define HELP_HINT                                                            \
"Use \"-h\" to get help."

/* Default width and height of characters in input font */
#define DEF_CWI 8
#define DEF_CHI 8
/* Default count of characters per line in output image */
#define DEF_CPL 16

/* Options */
const char *opt_fi = NULL;  /* filename (input) */
const char *opt_fo = NULL;  /* filename (output) */
unsigned opt_cwi = DEF_CWI; /* character width (input) */
unsigned opt_chi = DEF_CHI; /* character height (input) */
unsigned opt_cwo = 0; /* character width (output) - same as input by default */
unsigned opt_cho = 0; /* character height (output) - same as input by default */
unsigned opt_cpl = 0; /* characters per line count for output (autodetect) */
bool opt_help = false;
bool opt_encode = false;
bool opt_decode = false;
bool opt_vertical = false;
bool opt_verbose = true;

void show_usage () {
  printf (
    PROGRAM " version " VERSION " - " DESCRIPTION "\n"
    COPYRIGHT "\n"
    LICENSE "\n"
    "Home page: <" HOMEPAGE ">\n"
  );
  printf (
    "Third-party software used:\n"
    THIRDPARTY "\n"
  );
  printf (
    "\n"
    "Usage:\n"
    "  " PROGRAM " [options ...] input output\n"
  );
  printf (
    "\n"
    "Options:\n"
    "  -h      Show this help\n"
    "  -q      Quiet output (default is verbose output)\n"
    "  -d      Decode binary font file into an image (default is encode mode)\n"
    "  -r      Rotate input characters for vertical output\n"
    "  -f WxH  Input character size: W=width, H=height (default is %ux%u)\n"
    "  -t WxH  Output character size: W=width, H=height (defaults to \"-f\")\n"
    "  -c N    Output N characters per line in output image (default is %u)\n",
    (unsigned) DEF_CWI,
    (unsigned) DEF_CHI,
    (unsigned) DEF_CPL
  );
  printf (
    "\n"
    "  input   Input PNG image file (or binary font file in \"-d\" mode)\n"
    "  output  Output binary font file (or PNG image in \"-d\" mode)\n"
  );
  printf (
    "\n"
    "Input image width must be a multiple of input character's width.\n"
    "Input image height must be a multiple of input character's height.\n"
    "Input and output color for background is pure black (i.e. R=G=B=0).\n"
    "Input ink color is any other. Output ink color is pure white.\n"
  );
}

#include "errors.c"
#include "common.c"
#include "palette.c"
#include "image.c"
#include "font.c"

/* Returns 0 on success, -1 on error */
int parse_arg_long (long *a, const char *arg) {
  long _a;
  char *endptr;

  _a = strtol (arg, &endptr, 10);
  if (errno || !endptr) return -1;

  *a = _a;
  return 0;
}

/* Returns 0 on success, -1 on error */
int parse_arg_long_x_long (long *a, long *b, const char *arg) {
  long _a, _b;
  char *xptr, *endptr;

  xptr = strchr (arg, 'x');
  if (!xptr || (xptr == arg)) return -1;

  _a = strtol (arg, &endptr, 10);
  if (errno || (endptr != xptr)) return -1;

  _b = strtol (xptr + 1, &endptr, 10);
  if (errno || !endptr) return -1;

  *a = _a;
  *b = _b;
  return 0;
}

/* Returns 0 on success, -1 on error, 1 if no arguments */
int parse_args (int argc, char *argv[]) {
  int i, f;

  if (argc == 1) return 1;

  i = 1;
  f = 0;
  while (i < argc) {
    if (argv[i][0] == '-') {
      if (strcmp (&argv[i][1], "h") == 0) opt_help = true;
      else if (strcmp (&argv[i][1], "q") == 0) opt_verbose = false;
      else if (strcmp (&argv[i][1], "d") == 0) opt_decode = true;
      else if (strcmp (&argv[i][1], "r") == 0) opt_vertical = true;
      else if (strcmp (&argv[i][1], "f") == 0) {
        long a, b;
        if (i + 1 == argc) error_missing_arg (argv[i], i);

        if (parse_arg_long_x_long (&a, &b, argv[i + 1])
        ||  (a <= 0) || (b <= 0)) {
          error_bad_arg (argv[i], i);
          return -1;
        }

        opt_cwi = a;
        opt_chi = b;
        i++;

      } else if (strcmp (&argv[i][1], "t") == 0) {
        long a, b;
        if (i + 1 == argc) error_missing_arg (argv[i], i);

        if (parse_arg_long_x_long (&a, &b, argv[i + 1])
        ||  (a < 0) || (b < 0)) {
          error_bad_arg (argv[i], i);
          return -1;
        }

        if (a) opt_cwo = a;
        if (b) opt_cho = b;
        i++;

      } else if (strcmp (&argv[i][1], "c") == 0) {
        long a;
        if (i + 1 == argc) error_missing_arg (argv[i], i);

        if (parse_arg_long (&a, argv[i + 1])
        ||  (a <= 0)) {
          error_bad_arg (argv[i], i);
          return -1;
        }

        opt_cpl = a;
        i++;

      } else {
        error ("Unknown option \"%s\" (argument %u)", argv[i], i);
        return -1;
      }

    } else {
      switch (f++) {
        case 0: opt_fi = argv[i]; break;
        case 1: opt_fo = argv[i]; break;
        default:
          error ("Extra parameter \"%s\" given (argument %u)", argv[i], i);
          return -1;
      }
    }
    i++;
  }

  return 0;
}

/* import_png => "image" => encode => "font" => save */
/* Returns 0 on success, other value on error */
int encode () {
  int status; /* exit status */
  struct image_t *image = NULL; /* allocated locally */
  struct font_t *font = NULL;   /* allocated locally */
  const char *in_type_str, *out_type_str;

  /* save for later use */
  in_type_str = get_image_type_str (IMGT_PNG);

  status = import_image_from_file_png (&image, ICT_GREY, 8, opt_fi);
  if (status) {
    error ("Failed to import %s from file \"%s\" (%s)",
      in_type_str,
      opt_fi,
      image_error_text ());
    goto error_exit;
  }

  /* save for later use */
  out_type_str = get_font_type_str ();

  status = import_font_from_image (
    &font, opt_cwo, opt_cho, image, opt_cwi, opt_chi, opt_vertical);
  if (status) {
    error ("Failed to import %s from %s (%s)",
      out_type_str,
      get_image_type_str (image->type),
      font_error_text ());
    goto error_exit;
  }

  image_dispose (&image);

  status = font_save_as (font, opt_fo);
  if (status) {
    error ("Failed to save %s as file \"%s\" (%s)",
      out_type_str,
      opt_fi,
      font_error_text ());
    goto error_exit;
  }

  font_dispose (&font);

  if (opt_verbose)
    message ("Converted %s file \"%s\" into %s file \"%s\"",
      in_type_str,
      opt_fi,
      out_type_str,
      opt_fo);
  return 0;

error_exit:
  if (image) image_dispose (&image);
  if (font) font_dispose (&font);
  return status;
}

/* load => "font" => decode => "image" => convert_to_png => "png" => save */
/* Returns 0 on success, other value on error */
int decode () {
  int status; /* exit status */
  struct font_t *font = NULL;   /* allocated locally */
  struct image_t *image = NULL; /* allocated locally */
  struct image_t *png = NULL;   /* allocated locally */
  const char *in_type_str, *out_type_str;

  /* save for later use */
  in_type_str = get_font_type_str ();

  status = import_font_from_file (&font, opt_fi, opt_cwi, opt_chi);
  if (status) {
    error ("Failed to import %s from file \"%s\" (%s)",
      in_type_str,
      opt_fi,
      font_error_text ());
    goto error_exit;
  }

  image = export_font_as_image (
    font, ICT_GREY, 8, opt_cwo, opt_cho, opt_cpl, DEF_CPL, opt_vertical);
  if (!image) {
    error ("Failed to export %s as %s (%s)",
      in_type_str,
      get_image_type_str (IMGT_RAW),
      font_error_text ());
    status = -1;
    goto error_exit;
  }

  font_dispose (&font);

  /* save for later use */
  out_type_str = get_image_type_str (IMGT_PNG);

  png = export_image_as_image_png (image);
  if (!png) {
    error ("Failed to export %s as %s (%s)",
      get_image_type_str (IMGT_RAW),
      out_type_str,
      image_error_text ());
    status = -1;
    goto error_exit;
  }

  image_dispose (&image);

  status = image_save_as (png, opt_fo);
  if (status) {
    error ("Failed to save %s as file \"%s\" (%s)",
      out_type_str,
      opt_fo,
      image_error_text ());
    goto error_exit;
  }

  image_dispose (&png);

  if (opt_verbose)
    message ("Converted %s file \"%s\" into %s file \"%s\"",
      in_type_str,
      opt_fi,
      out_type_str,
      opt_fo);
  return 0;

error_exit:
  if (font) font_dispose (&font);
  if (image) image_dispose (&image);
  if (png) image_dispose (&png);
  return status;
}

int main (int argc, char **argv) {
  int err;

  err = parse_args (argc, argv);
  switch (err) {
    case 0: break;
    case 1:
      message ("No parameters. " HELP_HINT);
      return 0;
    default:
      return -1;
  }

  if (opt_help) {
    show_usage ();
    return 0;
  }

  if (!opt_fi) {
    error ("No %s specified", "input filename");
    return -1;
  }
  if (!opt_fo) {
    error ("No %s specified", "output filename");
    return -1;
  }

  if (!opt_cwo) opt_cwo = opt_cwi;
  if (!opt_cho) opt_cho = opt_chi;

  if (opt_decode) {
    if (decode ()) return -1;
  } else {
    if (encode ()) return -1;
  }

  return 0;
}
