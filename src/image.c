/*
 * image.c - routines to deal with a PNG image.
 *
 * Copyright (C) 2021-2024 Ivan Tatarinov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: Copyright (C) 2021-2024 Ivan Tatarinov
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include "palette.h"
#include "image.h"

/* Flags for error message of the last image-routine */
#define IMGEF_MEM_MASK  (1<<0)
#define IMGEF_MEM_CONST (0<<0)
#define IMGEF_MEM_VAR   (1<<0)

/* An error message of a last failed image-routine */
static const char *image_err = NULL;
static unsigned image_err_flags = IMGEF_MEM_CONST;

/* Image types and parameters */
static const struct {
  /* key: type (unique) */
  const char *type_str;
  image_type_t type;
} image_types[] = {
  { "raw image",
    IMGT_RAW },
  { "PNG image",
    IMGT_PNG },
  { NULL, 0 }
};

static void image_err_assign_const (const char *text) {
  image_err = text;
  image_err_flags = IMGEF_MEM_CONST;
}

static void image_err_assign_var (char *text) {
  image_err = text;
  image_err_flags = IMGEF_MEM_VAR;
}

/* Free previous error message */
static void image_err_free () {
  if (image_err
  &&  ((image_err_flags & IMGEF_MEM_MASK) == IMGEF_MEM_VAR))
    free ((void *) image_err);
}

static void image_error (const char *text) {
  image_err_free ();
  image_err_assign_const (text);
}

/* Formatted version of image_error() */
static void image_error_fmt (const char *fmt, ...) {
  va_list ap;
  int n;
  size_t size = 0;
  char *s = NULL;

  image_err_free ();

  /* Determine required size */
  va_start (ap, fmt);
  n = vsnprintf (s, size, fmt, ap);
  va_end (ap);

  if (n < 0) {
    image_err_assign_const ("Internal failure in vsnprintf()");
    return;
  }

  /* One extra byte for '\0' */
  size = (size_t) n + 1;
  s = malloc (size);
  if (!s) {
    image_err_assign_const ("Memory allocation failed");
    return;
  }

  va_start (ap, fmt);
  n = vsnprintf (s, size, fmt, ap);
  va_end (ap);

  if (n < 0) {
    free (s);
    image_err_assign_const ("Internal failure in vsnprintf()");
    return;
  }

  image_err_assign_var (s);
}

const char *image_error_text () {
  return image_err;
}

const char *get_image_type_str (image_type_t type) {
  int i;

  for (i = 0; image_types[i].type_str; i++)
    if (image_types[i].type == type)
      return image_types[i].type_str;

  image_error ("Image has unknown internal format");
  return "unknown type of image";
}

size_t get_raw_image_size (enum image_color_type_t ct, unsigned bd,
  unsigned w, unsigned h) {

  switch (ct) {
    case ICT_GREY:
      if (bd == 8) return w * h;
      break;
    case ICT_RGBA:
      if (bd == 8) return (w * h) << 2;
      break;
    default:
      break;
  }

  image_error ("Image has unknown internal format");
  return 0;
}

struct image_t *image_new () {
  struct image_t *self = calloc (1, sizeof (struct image_t));
  if (!self) image_error ("Memory allocation failed");
  return self;
}

struct image_t *image_dup (struct image_t *self) {
  struct image_t *image = image_new ();
  if (!image) {
    image_error ("Memory allocation failed");
    goto error_exit;
  }

  image->type = self->type;
  image->colortype = self->colortype;
  image->bitdepth = self->bitdepth;
  /*image->data = NULL;*/ /* not needed */
  if (self->data && self->size) {
    image->data = malloc (self->size);
    if (!image->data) {
      image_error ("Memory allocation failed");
      goto error_exit;
    }
    memcpy (image->data, self->data, self->size);
  }
  image->size = self->size;
  image->w = self->w;
  image->h = self->h;
  /*image->filename = NULL;*/ /* not needed */
  if (self->filename)
    if (str_dup (&image->filename, self->filename)) {
      image_error ("Memory allocation failed");
      goto error_exit;
    }

  return image;

error_exit:
  if (image) image_dispose (&image);
  return NULL;
}

void image_free (struct image_t *self) {
  if (self->filename) {
    free (self->filename);
    self->filename = NULL;
  }
  if (self->data) {
    free (self->data);
    self->data = NULL;
  }
}

void image_dispose (struct image_t **self) {
  if (*self) {
    image_free (*self);
    free (*self);
    *self = NULL;
  }
}

void image_assign_raw (struct image_t *self, enum image_color_type_t ct,
  unsigned bd, unsigned char *data, size_t size, unsigned w, unsigned h) {

    self->type = IMGT_RAW;
    self->colortype = ct;
    self->bitdepth = bd;
    self->data = data;
    self->size = size;
    self->w = w;
    self->h = h;
}

void image_assign_png (struct image_t *self, unsigned char *data,
  size_t size, unsigned w, unsigned h) {

    self->type = IMGT_PNG;
    self->colortype = 0;
    self->bitdepth = 0;
    self->data = data;
    self->size = size;
    self->w = w;
    self->h = h;
}

/* Returns 0 on success, -1 on error */
int image_set_filename (struct image_t *self, const char *filename) {
  int status = str_dup (&self->filename, filename);
  if (status) image_error ("Memory allocation failed");
  return status;
}

/* Returns 0 on success, -1 on error */
int image_format_to_lodepng (struct image_t *self,
  struct image_format_lodepng_t *out) {

  switch (self->colortype) {
    case ICT_GREY:
      if (self->bitdepth == 8) {
        out->colortype = LCT_GREY;
        out->bitdepth = self->bitdepth;
        return 0;
      }
      break;
    case ICT_RGBA:
      if (self->bitdepth == 8) {
        out->colortype = LCT_RGBA;
        out->bitdepth = self->bitdepth;
        return 0;
      }
      break;
    default:
      break;
  }

  image_error ("Image has unknown internal format");
  return -1;
}

/* Returns 0 on success, -1 on error */
int image_convert_to_png (struct image_t *self, struct image_t *image) {
  struct image_format_lodepng_t fmt;
  unsigned char *png_data;
  size_t png_size;
  unsigned err;

  if (image->type != IMGT_RAW) {
    image_error ("Image has wrong type");
    return -1;
  }

  if (image_format_to_lodepng (image, &fmt)) return -1;

  if (!(image->data && image->size)) {
    image_error ("Image has empty data");
    return -1;
  }

  err = lodepng_encode_memory (&png_data, &png_size,
    image->data, image->w, image->h, fmt.colortype, fmt.bitdepth);
  if (err) {
    image_error (lodepng_error_text (err));
    return -1;
  }

  image_assign_png (self, png_data, png_size, image->w, image->h);
  return 0;
}

/* Returns 0 on success, other value on error */
int image_save_as (struct image_t *self, const char *filename) {
  int status = file_save (self->data, self->size, filename);
  if (status) image_error ("Failed to write output file");
  return status;
}

/* Returns 0 on success, 1 if not a PNG image, 2 if PNG decoder failed, other
   value if other error occurred. */
int import_image_from_memory_png (struct image_t **self,
  enum image_color_type_t ct, unsigned bd, unsigned char *data, size_t size) {

  int status; /* exit status */
  struct image_t *image = NULL;
  struct image_format_lodepng_t fmt;
  unsigned char *raw_data;
  unsigned w, h;
  unsigned err;

  image = image_new ();
  if (!image) { status = -1; goto error_exit; }

  image->colortype = ct;
  image->bitdepth = bd;

  status = image_format_to_lodepng (image, &fmt);
  if (status) goto error_exit;

  /* Try to decode PNG image */
  err = lodepng_decode_memory (&raw_data, &w, &h, data, size,
    fmt.colortype, fmt.bitdepth);
  switch (err) {
    case 0: break;

    case 27: case 28: case 29: case 48: case 94:
      image_error_fmt ("Not a PNG image (%s)", lodepng_error_text (err));
      status = 1;
      goto error_exit;

    default:
      image_error_fmt ("PNG decoder failed (%s)", lodepng_error_text (err));
      status = 2;
      goto error_exit;
    }

  image_assign_raw (image, ct, bd, raw_data,
    get_raw_image_size (ct, bd, w, h), w, h);
  *self = image;
  return 0;

error_exit:
  if (image) image_dispose (&image);
  if (raw_data) free (raw_data);
  return status;
}

/* Returns 0 on success, 1 if not a PNG image, 2 if PNG decoder failed, other
   value if other error occurred. */
int import_image_from_file_png (struct image_t **self,
  enum image_color_type_t ct, unsigned bd, const char *filename) {

  int status; /* exit_status */
  struct image_t *image = NULL;
  struct image_format_lodepng_t fmt;
  unsigned char *raw_data = NULL;
  unsigned w, h;
  unsigned err;

  image = image_new ();
  if (!image) { status = -1; goto error_exit; }

  image->colortype = ct;
  image->bitdepth = bd;

  status = image_format_to_lodepng (image, &fmt);
  if (status) goto error_exit;

  /* Try to decode PNG image */
  err = lodepng_decode_file (&raw_data, &w, &h, filename,
    fmt.colortype, fmt.bitdepth);
  switch (err) {
    case 0: break;

    case 27: case 28: case 29: case 48: case 94:
      image_error_fmt ("Not a PNG image (%s)", lodepng_error_text (err));
      status = 1;
      goto error_exit;

    default:
      image_error_fmt ("PNG decoder error (%s)", lodepng_error_text (err));
      status = 2;
      goto error_exit;
    }

  status = image_set_filename (image, filename);
  if (status) goto error_exit;

  image_assign_raw (image, ct, bd, raw_data,
    get_raw_image_size (ct, bd, w, h), w, h);
  *self = image;
  return 0;

error_exit:
  if (image) image_dispose (&image);
  if (raw_data) free (raw_data);
  return status;
}

struct image_t *export_image_as_image_png (struct image_t *self) {
  struct image_t *image = NULL;

  if (self->type == IMGT_PNG) {
    image = image_dup (self);
    if (!image) goto error_exit;

  } else {
    image = image_new ();
    if (!image) goto error_exit;

    if (image_convert_to_png (image, self)) goto error_exit;
  }

  return image;

error_exit:
  if (image) image_dispose (&image);
  return NULL;
}
