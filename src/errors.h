/*
 * errors.h - header file for "errors.c".
 *
 * Copyright (C) 2021 Ivan Tatarinov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: Copyright (C) 2021 Ivan Tatarinov
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef _ERRORS_H_INCLUDED
#define _ERRORS_H_INCLUDED

#include <stdarg.h>

void message (const char *format, ...);
void error (const char *format, ...);
void error_missing_arg (const char *name, unsigned index);
void error_bad_arg (const char *name, unsigned index);
void error_malloc (size_t size);

#endif  /* !_ERRORS_H_INCLUDED */
