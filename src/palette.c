/*
 * palette.c - ZX Spectrum's color palette.
 *
 * Copyright (C) 2021 Ivan Tatarinov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: Copyright (C) 2021 Ivan Tatarinov
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "palette.h"

#define B_0 0xc0
#define B_1 0xff
#define ZXRGBA32(b, c) (b*(((c&2)/2) + ((c&4)<<6) + ((c&1)<<16)) + 0xff000000UL)

const uint32_t ZXPALETTE[16] = {
  /* bright 0 */
  ZXRGBA32 (B_0, 0), /* black */
  ZXRGBA32 (B_0, 1), /* blue */
  ZXRGBA32 (B_0, 2), /* red */
  ZXRGBA32 (B_0, 3), /* magenta */
  ZXRGBA32 (B_0, 4), /* green */
  ZXRGBA32 (B_0, 5), /* cyan */
  ZXRGBA32 (B_0, 6), /* yellow */
  ZXRGBA32 (B_0, 7), /* white */
  /* bright 1 */
  ZXRGBA32 (B_1, 0), /* black */
  ZXRGBA32 (B_1, 1), /* blue */
  ZXRGBA32 (B_1, 2), /* red */
  ZXRGBA32 (B_1, 3), /* magenta */
  ZXRGBA32 (B_1, 4), /* green */
  ZXRGBA32 (B_1, 5), /* cyan */
  ZXRGBA32 (B_1, 6), /* yellow */
  ZXRGBA32 (B_1, 7), /* white */
};

/* Returns 0 on success, -1 on error */
int find_zx_color_RGBA8 (unsigned *index, uint32_t color) {
  unsigned char r, g, b;
  uint32_t c;
  unsigned i;

  r = (color & 0xff);
  if (r < 8) r = 0;
  else if ((r >= 128) && (r < 240)) r = B_0;
  else if (r >= 240) r = B_1;
  else return -1; /* wrong component's value */
  g = ((color >> 8) & 0xff);
  if (g < 8) g = 0;
  else if ((g >= 128) && (g < 240)) g = B_0;
  else if (g >= 240) g = B_1;
  else return -1; /* wrong component's value */
  b = ((color >> 16) & 0xff);
  if (b < 8) b = 0;
  else if ((b >= 128) && (b < 240)) b = B_0;
  else if (b >= 240) b = B_1;
  else return -1; /* wrong component's value */

  c = r + (g << 8) + (b << 16) + 0xff000000UL;
  for (i = 0; i < 16; i++)
    if (c == ZXPALETTE[i]) {
      *index = i;
      return 0;
    }

  return -1; /* not found in palette */
}
