/*
 * errors.c - routines to print common error messages.
 *
 * Copyright (C) 2021 Ivan Tatarinov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: Copyright (C) 2021 Ivan Tatarinov
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "errors.h"

void message (const char *format, ...) {
  va_list ap;

  va_start (ap, format);
  fprintf (stderr, PROGRAM ": ");
  vfprintf (stderr, format, ap);
  fprintf (stderr, "\n");
  va_end (ap);
}

void error (const char *format, ...) {
  va_list ap;

  va_start (ap, format);
  fprintf (stderr, PROGRAM ": ERROR: ");
  vfprintf (stderr, format, ap);
  fprintf (stderr, "\n");
  va_end (ap);
}

void error_missing_arg (const char *name, unsigned index) {
  error ("Missing parameter for \"%s\" (argument %u).", name, index);
}

void error_bad_arg (const char *name, unsigned index) {
  error ("Bad parameter value for \"%s\" (argument %u).", name, index);
}

void error_malloc (size_t size) {
  error ("Failed to allocate %lu bytes of memory", size);
}
