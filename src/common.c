/*
 * common.c - common routines.
 *
 * Copyright (C) 2021 Ivan Tatarinov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: Copyright (C) 2021 Ivan Tatarinov
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "common.h"

/* Returns 0 on success, -1 on error */
int str_dup (char **dest, const char *src) {
  bool differs = *dest && src && strcmp (*dest, src);

  if ((*dest && !src) || differs) {
    free (*dest);
    *dest = NULL;
  }

  if ((!*dest && src) || differs) {
    unsigned len = strlen (src);
    *dest = malloc (len + 1);
    if (!*dest) {
      error_malloc (len + 1);
      return -1;
    }
    if (len)
      memcpy (*dest, src, len);
    (*dest)[len] = '\0';
  }

  return 0;
}

/* Returns 0 on success, -1 on error */
int file_load (unsigned char **out_data, size_t *out_size,
  const char *filename) {

  FILE *f;
  unsigned char *data = NULL;
  size_t size;

  f = fopen (filename, "rb");
  if (!f) {
    error ("Failed to open file \"%s\"", filename);
    goto error_exit;
  }

  if (fseek (f, 0, SEEK_END)) {
    error ("Failed to seek in file \"%s\"", filename);
    goto error_exit;
  }

  size = ftell (f);
  if (errno) {
    error ("Failed to get file \"%s\" size", filename);
    goto error_exit;
  }

  if (size) {
    data = malloc (size);
    if (!data) {
      error_malloc (size);
      goto error_exit;
    }

    rewind (f);

    if (!fread (data, size, 1, f)) {
      error ("Failed to read file \"%s\"", filename);
      goto error_exit;
    }
  }

  fclose (f);

  *out_data = data;
  *out_size = size;
  return 0;

error_exit:
  if (f) fclose (f);
  if (data) free (data);
  return -1;
}

/* Returns 0 on success, -1 on error */
int file_save (unsigned char *data, size_t size, const char *filename) {
  FILE *f;
  int err;

  f = fopen (filename, "w+b");
  if (!f) {
    error ("Failed to create file \"%s\"", filename);
    return -1;
  }

  if (size && !fwrite (data, size, 1, f)) {
    error ("Failed to write file \"%s\"", filename);
    err = -1;
  } else
    err = 0;

  fclose (f);
  return err;
}
