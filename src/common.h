/*
 * common.h - header file for "common.c".
 *
 * Copyright (C) 2021 Ivan Tatarinov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: Copyright (C) 2021 Ivan Tatarinov
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef _COMMON_H_INCLUDED
#define _COMMON_H_INCLUDED

/* Miscellaneous */

int str_dup (char **dest, const char *src);
int file_load (unsigned char **out_data, size_t *out_size,
  const char *filename);
int file_save (unsigned char *data, size_t size, const char *filename);

#endif  /* !_COMMON_H_INCLUDED */
