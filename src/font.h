/*
 * font.h - header file for "font.c".
 *
 * Copyright (C) 2021 Ivan Tatarinov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: Copyright (C) 2021 Ivan Tatarinov
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef _FONT_H_INCLUDED
#define _FONT_H_INCLUDED

#define FNT_CHAR_SIZE(w, h) ((((w)+7)>>3)*(h))

struct font_t {
  unsigned char *data;  /* font data */
  size_t size;  /* font data size */
  unsigned cw;  /* character width */
  unsigned ch;  /* character height */
  char *filename;
};

#include "image.h"

/* Get an error message of a last failed font-routine */
const char *font_error_text ();

const char *get_font_type_str (/*nothing for now*/);

struct font_t *font_new ();
struct font_t *font_dup (struct font_t *self);
void font_free (struct font_t *self);
void font_dispose (struct font_t **self);
void font_assign (struct font_t *self, unsigned char *data, size_t size,
  unsigned cw, unsigned ch);
int font_set_filename (struct font_t *self, const char *filename);
int font_save_as (struct font_t *self, const char *filename);

int import_font_from_memory (struct font_t **self, unsigned char *data,
  size_t size, unsigned cw, unsigned ch);
int import_font_from_file (struct font_t **self, const char *filename,
  unsigned cw, unsigned ch);
int import_font_from_image (struct font_t **self, unsigned cwo, unsigned cho,
  struct image_t *image, unsigned cwi, unsigned chi, bool rotate);

struct image_t *export_font_as_image (struct font_t *self,
  enum image_color_type_t colortype, unsigned bitdepth,
  unsigned cwo, unsigned cho, unsigned wco, unsigned def_wco, bool rotate);

#endif  /* !_FONT_H_INCLUDED */
