/*
 * scrconv - converter between a PNG image, a binary bitmap image, a ZX
 * Spectrum's screen and RCS files.
 *
 * Copyright (C) 2021-2024 Ivan Tatarinov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: Copyright (C) 2021-2024 Ivan Tatarinov
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if STATIC_BUILD
# include "lodepng.c"
#else
# include "lodepng.h"
#endif
#include "lodepng_ver.h"

#define PROGRAM "scrconv"
#define DESCRIPTION "PNG image <=> binary image file converter."
#define VERSION "0.1.2"
#define COPYRIGHT "Copyright (C) 2021-2024 Ivan Tatarinov"
#define LICENSE                                                              \
"This program is free software: you can redistribute it and/or modify\n"     \
"it under the terms of the GNU General Public License as published by\n"     \
"the Free Software Foundation, either version 3 of the License, or\n"        \
"(at your option) any later version."
#define HOMEPAGE "https://gitlab.com/ivan-tat/therighttools"
#define THIRDPARTY                                                           \
"* " LIB_LODEPNG_NAME " library (compiled with version " LIB_LODEPNG_VERSION ")\n" \
"  License: " LIB_LODEPNG_LICENSE ". " LIB_LODEPNG_COPYRIGHT "\n"            \
"  Home page: <" LIB_LODEPNG_HOMEPAGE ">"

#define HELP_HINT                                                            \
"Use \"-h\" to get help."

/* Types of target/file */
typedef int target_type_t;
#define TT_NONE -1
#define TT_IMAGE 0
#define TT_BITMAP 1
#define TT_SCREEN 2
#define TT_RCS 3

/* Options */
const char *opt_fi = NULL;  /* input filename */
const char *opt_fia = NULL; /* input filename with color attributes */
const char *opt_fo = NULL;  /* output filename */
const char *opt_foa = NULL; /* output filename with color attributes */
unsigned opt_w = 0; /* input image width (auto-detect) */
unsigned opt_h = 0; /* input image height (auto-detect) */
target_type_t opt_ti = TT_NONE; /* input target/file type (auto-detect) */
target_type_t opt_to = TT_NONE; /* output target/file type (auto-detect) */
bool opt_help = false;
bool opt_verbose = true;

/* Allocated object type */
typedef int object_type_t;
#define OT_NONE 0
#define OT_IMAGE 1
#define OT_BITMAP 2

void show_usage () {
  printf (
    PROGRAM " version " VERSION " - " DESCRIPTION "\n"
    COPYRIGHT "\n"
    LICENSE "\n"
    "Home page: <" HOMEPAGE ">\n"
  );
  printf (
    "Third-party software used:\n"
    THIRDPARTY "\n"
  );
  printf (
    "\n"
    "Usage:\n"
    "  " PROGRAM " [options ...] [--] input output [attrs]\n"
  );
  printf (
    "\n"
    "Options:\n"
    "  -h       Show this help\n"
    "  -q       Quiet output (default is verbose output)\n"
    "  -s WxH   Image size: W=width, H=height (default is autodetect)\n"
    "  -f type  Input file type (default is autodetect)\n"
    "  -t type  Output file type (default depends on input type)\n"
    "  -a file  Use this file with color attributes to process input image\n"
    "  --       Stop parsing options, treat next arguments as filenames\n"
  );
  printf (
    "\n"
    "  input    Input file\n"
    "  output   Output file\n"
    "  attrs    Output file with color attributes\n"
  );
  printf (
    "\n"
    "Where \"type\" is:\n"
    "  image, png   PNG image\n"
    "  bitmap, bm   ZX Spectrum's bitmap data of any size and linear addressing\n"
    "               (with or without color attributes)\n"
    "  screen, scr  ZX Spectrum's screen 256x192 or it's part 256x64 or 256x128\n"
    "               (with or without color attributes)\n"
    "  rcs          ZX Spectrum's screen 256x192 or it's part 256x64 or 256x128\n"
    "               (RCS address encoding with or without color attributes).\n"
  );
  printf (
    "\n"
    "Default conversions (if only one option \"-f\" or \"-t\" given):\n"
    "  -f png <--> -t scr\n"
    "  -f bm    -> -t png\n"
    "  -f scr <--> -t png\n"
    "  -f rcs   -> -t png\n"
  );
  printf (
    "\n"
    "Flashing bit in color attributes is not supported for output PNG image.\n"
    "Width and height must be a multiple of 8.\n"
    "It is possible for \"bm\" image to be of any size (use \"-s\" option to specify).\n"
  );
}

#include "errors.c"

/* Returns 0 on success, other value on error */
int parse_arg_long (long *a, const char *arg) {
  long _a;
  char *endptr;

  _a = strtol (arg, &endptr, 10);
  if (errno || !endptr) return -1;

  *a = _a;
  return 0;
}

/* Returns 0 on success, other value on error */
int parse_arg_long_x_long (long *a, long *b, const char *arg) {
  long _a, _b;
  char *xptr, *endptr;

  xptr = strchr (arg, 'x');
  if (!xptr || (xptr == arg)) return -1;

  _a = strtol (arg, &endptr, 10);
  if (errno || (endptr != xptr)) return -1;

  _b = strtol (xptr + 1, &endptr, 10);
  if (errno || !endptr) return -1;

  *a = _a;
  *b = _b;
  return 0;
}

static const struct {
  /* key: name (unique) */
  char *name;
  int type;
} filetypes[] = {
  { "image",  TT_IMAGE },
  { "png",    TT_IMAGE },
  { "bitmap", TT_BITMAP },
  { "bm",     TT_BITMAP },
  { "screen", TT_SCREEN },
  { "scr",    TT_SCREEN },
  { "rcs",    TT_RCS },
  { NULL }
};

/* Returns 0 on success, other value on error */
int parse_arg_type (int *a, const char *arg) {
  int i;

  for (i = 0; filetypes[i].name; i++)
    if (!strcmp (arg, filetypes[i].name)) {
      *a = filetypes[i].type;
      return 0;
    }

  return -1;
}

/* Returns 0 on success, 1 if no arguments, other value on other error */
int parse_args (int argc, char *argv[]) {
  int i, f;
  bool optsend = false;

  if (argc == 1) return 1;

  i = 1;
  f = 0;
  while (i < argc) {
    if (!optsend && (argv[i][0] == '-')) {
      if (!strcmp (&argv[i][1], "-")) optsend = true;
      else if (!strcmp (&argv[i][1], "h")) opt_help = true;
      else if (!strcmp (&argv[i][1], "q")) opt_verbose = false;
      else if (!strcmp (&argv[i][1], "s")) {
        long a, b;
        if (i + 1 == argc) error_missing_arg (argv[i], i);

        if (parse_arg_long_x_long (&a, &b, argv[i + 1])
        ||  (a <= 0) || (a % 8) || (b <= 0) || (b % 8)) {
          error_bad_arg (argv[i], i);
          return -1;
        }

        opt_w = a;
        opt_h = b;
        i++;

      } else if (!strcmp (&argv[i][1], "f")) {
        int a;
        if (i + 1 == argc) error_missing_arg (argv[i], i);

        if (parse_arg_type (&a, argv[i + 1])
        ||  (a < 0)) {
          error_bad_arg (argv[i], i);
          return -1;
        }

        opt_ti = a;
        i++;

      } else if (!strcmp (&argv[i][1], "t")) {
        int a;
        if (i + 1 == argc) error_missing_arg (argv[i], i);

        if (parse_arg_type (&a, argv[i + 1])
        ||  (a < 0)) {
          error_bad_arg (argv[i], i);
          return -1;
        }

        opt_to = a;
        i++;

      } else if (!strcmp (&argv[i][1], "a")) {
        if (i + 1 == argc) error_missing_arg (argv[i], i);
        opt_fia = argv[i + 1];
        i++;

      } else {
        error ("Unknown option \"%s\" (argument %u)", argv[i], i);
        return -1;
      }

    } else {
      switch (f++) {
        case 0: opt_fi = argv[i]; break;
        case 1: opt_fo = argv[i]; break;
        case 2: opt_foa = argv[i]; break;
        default:
          error ("Extra parameter \"%s\" given (argument %u)", argv[i], i);
          return -1;
      }
    }
    i++;
  }

  return 0;
}

#include "common.h"
#include "common.c"
#include "palette.h"
#include "palette.c"
#include "image.h"
#include "image.c"
#include "bitmap.h"
#include "bitmap.c"

static const struct {
  /* key: target (unique) */
  target_type_t target;
  object_type_t object;
} target_object_types[] = {
  { TT_IMAGE,  OT_IMAGE },
  { TT_BITMAP, OT_BITMAP },
  { TT_SCREEN, OT_BITMAP },
  { TT_RCS,    OT_BITMAP },
  { -1, -1 }
};

/* OT_BITMAP default parameters */
static const struct {
  /* key: target (unique) */
  target_type_t target;
  bitmap_flags_t adr;
} target_bitmap_params[] = {
  { TT_BITMAP, BMF_ADR_LINEAR },
  { TT_SCREEN, BMF_ADR_SCREEN },
  { TT_RCS,    BMF_ADR_RCS },
  { -1, 0 }
};

object_type_t get_target_object_type (target_type_t target) {
  int i;

  for (i = 0; target_object_types[i].target >= 0; i++)
    if (target_object_types[i].target == target)
      return target_object_types[i].object;

  return -1;
}

target_type_t get_image_target (/*struct image_t *image*/) {
  return TT_IMAGE;
}

target_type_t get_bitmap_target (struct bitmap_t *bitmap) {
  bitmap_flags_t adr = bitmap->flags & BMF_ADR_MASK;
  int i;

  for (i = 0; target_bitmap_params[i].target >= 0; i++)
    if (target_bitmap_params[i].adr == adr)
      return target_bitmap_params[i].target;

  return -1;
}

bitmap_flags_t get_target_bitmap_address_mode (target_type_t target) {
  int i;

  for (i = 0; target_bitmap_params[i].target >= 0; i++)
    if (target_bitmap_params[i].target == target)
      return target_bitmap_params[i].adr;

  return 0;
}

/****************************************************************************/

/* Returns 0 on success, other value on error. */
int attributes_load (unsigned char **attrs, size_t *size,
  const char *filename, unsigned w, unsigned h) {

  int status; /* exit status */
  unsigned char *a = NULL;
  size_t s;

  status = file_load (&a, &s, filename);
  if (status) goto error_exit;
  /* check size */
  if (s != BM_ATTRS_SIZE (w, h)) {
    error ("File with color attributes \"%s\" has wrong size",
      filename);
    status = -1;
    goto error_exit;
  }

  *attrs = a;
  *size = s;
  return 0;

error_exit:
  if (a) free (a);
  return status;
}

/* If "in_bitmap" is given - "in_sf", "in_flags", "in_w", "in_h" arguments are
   ignored. */
/* Returns 0 on success, other value on error */
int convert_bitmap_to_image (
  const char *out_f,
  const char *out_fa,
  const char *in_f,
  bitmap_setup_flags_t in_sf, bitmap_flags_t in_flags,
  unsigned in_w, unsigned in_h,
  struct bitmap_t *in_bitmap,
  const char *in_fa) {

  int status; /* exit status */
  struct bitmap_t *bitmap = NULL; /* may be allocated locally */
  char *_in_f = NULL;             /* allocated locally */
  unsigned char *attrs = NULL;    /* allocated locally */
  size_t attrs_size;
  struct image_t *image = NULL;   /* allocated locally */
  struct image_t *png = NULL;     /* allocated locally */
  const char *in_type_str, *out_type_str;

  if (in_bitmap) {
    /* use loaded file, retrieve parameters out of it */
    bitmap = in_bitmap;
  } else {
    /* no file loaded, use user-specified parameters */
    status = import_bitmap_from_file (
      &bitmap, in_f, in_sf, in_flags, in_w, in_h);
    if (status) {
      error ("Failed to import %s from file \"%s\" (%s)",
        get_bitmap_type_str (in_sf, in_flags),
        in_f,
        bitmap_error_text ());
      goto error_exit;
    }
  }

  /* save for later use */
  if (str_dup (&_in_f, bitmap->filename)) {
    error ("Memory allocation failed");
    status = -1;
    goto error_exit;
  }
  in_type_str = get_bitmap_type_str (BMSF_ADR | BMSF_ATTRS, bitmap->flags);

  /* we got image parameters - safe to load attributes if any */
  if (in_fa) {
    status = attributes_load (
      &attrs, &attrs_size, in_fa, bitmap->w, bitmap->h);
    if (status) goto error_exit;
  }

  image = export_bitmap_as_image (bitmap, ICT_RGBA, 8, attrs);
  if (!image) {
    error ("Failed to export %s as %s (%s)",
      in_type_str,
      get_image_type_str (IMGT_RAW),
      bitmap_error_text ());
    status = -1;
    goto error_exit;
  }

  if (out_fa) {
    status = bitmap_save_attributes_as (bitmap, out_fa);
    if (status) {
      error ("Failed to save attributes of %s as file \"%s\" (%s)",
        in_type_str,
        out_fa,
        bitmap_error_text ());
      goto error_exit;
    }
  }

  if (!in_bitmap)
    bitmap_dispose (&bitmap);

  png = export_image_as_image_png (image);
  if (!png) {
    error ("Failed to export %s as %s (%s)",
      get_image_type_str (image->type),
      get_image_type_str (IMGT_PNG),
      image_error_text ());
    status = -1;
    goto error_exit;
  }

  image_dispose (&image);

  /* save for later use */
  out_type_str = get_image_type_str (png->type);

  status = image_save_as (png, out_f);
  if (status) {
    error ("Failed to save %s as file \"%s\" (%s)",
      out_type_str,
      out_f,
      image_error_text ());
    goto error_exit;
  }

  image_dispose (&png);

  if (attrs) {
    free (attrs);
    attrs = NULL; /* to prevent double free */
  }

  if (opt_verbose) {
    if (_in_f)
      message ("Converted %s file \"%s\" into %s file \"%s\"",
        in_type_str,
        _in_f,
        out_type_str,
        out_f);
    else
      message ("Converted %s into %s file \"%s\"",
        in_type_str,
        out_type_str,
        out_f);
  }

  if (_in_f) free (_in_f);
  return 0;

error_exit:
  if ((!in_bitmap) && bitmap) bitmap_dispose (&bitmap);
  if (_in_f) free (_in_f);
  if (attrs) free (attrs);
  if (image) image_dispose (&image);
  if (png) image_dispose (&png);
  return status;
}

/* If "in_image" is given - "in_f" argument is ignored */
/* Returns 0 on success, other value on error */
int convert_image_to_bitmap (
  const char *out_f,
  const char *out_fa,
  bitmap_setup_flags_t out_sf, bitmap_flags_t out_flags,
  /*unsigned out_w, unsigned out_h,*/
  const char *in_f,
  struct image_t *in_image,
  const char *in_fa) {

  int status; /* exit status */
  struct image_t *image = NULL;   /* may be allocated locally */
  unsigned char *attrs = NULL;    /* allocated locally */
  size_t attrs_size;
  struct bitmap_t *bitmap = NULL; /* allocated locally */
  char *_in_f = NULL;             /* allocated locally */
  const char *in_type_str, *out_type_str;

  if (in_image) {
    /* use loaded file, retrieve parameters out of it */
    image = in_image;
  } else {
    /* no file loaded, use user-specified parameters */
    status = import_image_from_file_png (&image, ICT_RGBA, 8, in_f);
    if (status) {
      error ("Failed to import %s from file \"%s\" (%s)",
        get_image_type_str (IMGT_PNG),
        in_f,
        image_error_text ());
      goto error_exit;
    }
  }

  /* save for later use */
  if (str_dup (&_in_f, image->filename)) {
    error ("Memory allocation failed");
    status = -1;
    goto error_exit;
  }
  in_type_str = get_image_type_str (IMGT_PNG);

  /* we got image parameters - safe to load attributes if any */
  if (in_fa) {
    status = attributes_load (
      &attrs, &attrs_size, in_fa, image->w, image->h);
    if (status) goto error_exit;
  }

  bitmap = bitmap_new ();
  if (!bitmap) {
    error ("Failed to create bitmap object (%s)",
      bitmap_error_text ());
    status = -1;
    goto error_exit;
  }

  status = import_bitmap_from_image (
    &bitmap, out_sf, out_flags, image->w, image->h, image, attrs);
  if (status) {
    error ("Failed to import %s from %s (%s)",
      get_bitmap_type_str (0, 0),
      get_image_type_str (image->type),
      bitmap_error_text ());
    goto error_exit;
  }

  if (!in_image)
    image_dispose (&image);

  if (attrs) {
    free (attrs);
    attrs = NULL; /* to prevent double free */
  }

  /* save for later use */
  out_type_str = get_bitmap_type_str (
    BMSF_ADR | BMSF_ATTRS, bitmap->flags);

  status = bitmap_save_as (bitmap, out_f);
  if (status) {
    error ("Failed to save %s as file \"%s\" (%s)",
      out_type_str,
      out_f,
      bitmap_error_text ());
    goto error_exit;
  }

  if (out_fa) {
    status = bitmap_save_attributes_as (bitmap, out_fa);
    if (status) {
      error ("Failed to save attributes of %s as file \"%s\" (%s)",
        out_type_str,
        out_f,
        bitmap_error_text ());
      goto error_exit;
    }
  }

  bitmap_dispose (&bitmap);

  if (opt_verbose) {
    if (_in_f)
      message ("Converted %s file \"%s\" into %s file \"%s\"",
        in_type_str,
        _in_f,
        out_type_str,
        out_f);
    else
      message ("Converted %s into %s file \"%s\"",
        in_type_str,
        out_type_str,
        out_f);
  }

  if (_in_f) free (_in_f);
  if (attrs) free (attrs);
  return 0;

error_exit:
  if ((!in_image) && image) image_dispose (&image);
  if (_in_f) free (_in_f);
  if (attrs) free (attrs);
  if (bitmap) bitmap_dispose (&bitmap);
  return status;
}

/* If "in_bitmap" is given - all other "in_*" arguments are ignored */
/* Returns 0 on success, other value on error */
int convert_bitmaps (
  const char *out_f,
  const char *out_fa,
  bitmap_setup_flags_t out_sf, bitmap_flags_t out_flags,
  unsigned out_w, unsigned out_h,
  const char *in_f,
  bitmap_setup_flags_t in_sf, bitmap_flags_t in_flags,
  unsigned in_w, unsigned in_h,
  struct bitmap_t *in_bitmap,
  const char *in_fa) {

  int status; /* exit status */
  struct bitmap_t *bitmap = NULL;     /* may be allocated locally */
  char *_in_f = NULL;                 /* allocated locally */
  unsigned char *attrs = NULL;        /* allocated locally */
  size_t attrs_size;
  struct bitmap_t *out_bitmap = NULL; /* allocated locally */
  const char *in_type_str, *out_type_str;

  if (in_bitmap) {
    /* use loaded file, retrieve parameters out of it */
    bitmap = in_bitmap;
  } else {
    /* no file loaded, use user-specified parameters */
    status = import_bitmap_from_file (
      &bitmap, in_f, in_sf, in_flags, in_w, in_h);
    if (status) {
      error ("Failed to import %s from file \"%s\" (%s)",
        get_bitmap_type_str (in_sf, in_flags),
        in_f,
        bitmap_error_text ());
      goto error_exit;
    }
  }

  /* save for later use */
  if (str_dup (&_in_f, bitmap->filename)) {
    error ("Memory allocation failed");
    status = -1;
    goto error_exit;
  }
  in_type_str = get_bitmap_type_str (BMSF_ADR | BMSF_ATTRS, bitmap->flags);

  /* get missing parameters of output bitmap from input bitmap */
  if (!(out_sf & BMSF_ADR)) {
    out_sf |= BMSF_ADR;
    out_flags = (out_flags & ~BMF_ADR_MASK) | (bitmap->flags & BMF_ADR_MASK);
  }
  if (!(out_sf & BMSF_ATTRS)) {
    out_sf |= BMSF_ATTRS;
    out_flags = (out_flags & ~BMF_ATTRS) | (bitmap->flags & BMF_ATTRS);
  }
  if (!(out_w && out_h)) {
    out_w = bitmap->w;
    out_h = bitmap->h;
  }

  /* we got bitmap parameters - safe to load attributes if any */
  if (in_fa) {
    status = attributes_load (
      &attrs, &attrs_size, in_fa, out_w, out_h);
    if (status) goto error_exit;
  }

  out_bitmap = export_bitmap_as_bitmap (
    bitmap, out_flags, out_w, out_h, attrs);
  if (!out_bitmap) {
    error ("Failed to convert bitmap (%s)",
      bitmap_error_text ());
    status = -1;
    goto error_exit;
  }

  if (!in_bitmap)
    bitmap_dispose (&bitmap);

  if (attrs) {
    free (attrs);
    attrs = NULL; /* to prevent double free */
  }

  /* save for later use */
  out_type_str = get_bitmap_type_str (
    BMSF_ADR | BMSF_ATTRS, out_bitmap->flags);

  status = bitmap_save_as (out_bitmap, out_f);
  if (status) {
    error ("Failed to save %s as file \"%s\" (%s)",
      out_type_str,
      out_f,
      bitmap_error_text ());
    goto error_exit;
  }

  if (out_fa) {
    status = bitmap_save_attributes_as (out_bitmap, out_fa);
    if (status) {
      error ("Failed to save attributes of %s as file \"%s\" (%s)",
        out_type_str,
        out_fa,
        bitmap_error_text ());
      goto error_exit;
    }
  }

  bitmap_dispose (&out_bitmap);

  if (opt_verbose) {
    if (_in_f)
      message ("Converted %s file \"%s\" into %s file \"%s\"",
        in_type_str,
        _in_f,
        out_type_str,
        out_f);
    else
      message ("Converted %s into %s file \"%s\"",
        in_type_str,
        out_type_str,
        out_f);
  }

  if (_in_f) free (_in_f);
  if (attrs) free (attrs);
  return 0;

error_exit:
  if ((!in_bitmap) && bitmap) bitmap_dispose (&bitmap);
  if (_in_f) free (_in_f);
  if (attrs) free (attrs);
  if (out_bitmap) bitmap_dispose (&out_bitmap);
  return status;
}

/****************************************************************************/

static const struct {
  target_type_t from, to;
} default_convertions[] = {
  { TT_IMAGE,  TT_SCREEN },
  { TT_BITMAP, TT_IMAGE },
  { TT_SCREEN, TT_IMAGE },
  { TT_RCS,    TT_IMAGE },
  { -1, -1 }
};

/* Returns 0 on success, other value on error */
int set_default_conversion (target_type_t *to, target_type_t from,
  const char *name) {

  int i;

  for (i = 0; default_convertions[i].from >= 0; i++)
    if (default_convertions[i].from == from) {
      *to = default_convertions[i].to;
      return 0;
    }

  error ("Unknown %s specified", name);
  return -1;
}

/* Tries to guess input file type */
/* Returns 0 on success, 1 if file format is not recognized, other value
   if other error. */
int load_file_try (void **out_object, target_type_t *out_type,
  const char *filename, unsigned w, unsigned h) {

  int status; /* exit status */
  unsigned char *data = NULL;
  size_t size;
  struct image_t *image = NULL;
  struct bitmap_t *bitmap = NULL;
  const char *in_type_str;

  status = file_load (&data, &size, filename);
  if (status) goto error_exit;

  /* Assume a PNG image */

  in_type_str = get_image_type_str (IMGT_PNG);
  status = import_image_from_memory_png (&image, ICT_RGBA, 8, data, size);
  switch (status) {
    case 0:
      free (data);
      data = NULL; /* to prevent double free */
      if (w && h) {
        if ((image->w != w) || (image->h != h)) {
          error ("Image size %ux%u of input file \"%s\" does not match "
            "specified size %ux%u", image->w, image->h, filename, w, h);
          status = -1;
          goto error_exit;
        }
      }
      if (image_set_filename (image, filename)) {
        error ("Failed to import %s from file \"%s\" (%s)",
          in_type_str,
          filename,
          image_error_text ());
        status = -1;
        goto error_exit;
      }
      /* Success */
      *out_object = (void *) image;
      *out_type = OT_IMAGE;
      return 0;

    case 1:
      /* Not a PNG image */
      break;

    default:
      error ("Failed to import %s from file \"%s\" (%s)",
        in_type_str,
        filename,
        image_error_text ());
      goto error_exit;
  }

  /* Assume a bitmap */

  in_type_str = get_bitmap_type_str (0, 0);
  status = import_bitmap_from_memory (&bitmap, 0, 0, data, size, w, h);
  switch (status) {
    case 0:
      free (data);
      data = NULL; /* to prevent double free */
      if (bitmap_set_filename (bitmap, filename)) {
        error ("Failed to import %s from file \"%s\" (%s)",
          in_type_str,
          filename,
          bitmap_error_text ());
        status = -1;
        goto error_exit;
      }
      /* Success */
      *out_object = (void *) bitmap;
      *out_type = OT_BITMAP;
      return 0;

    case 1:
      /* Not a bitmap */
      break;

    default:
      error ("Failed to import %s from file \"%s\" (%s)",
        in_type_str,
        filename,
        bitmap_error_text ());
      goto error_exit;
  }

  /* Unknown file format */
  return 1;

error_exit:
  if (data) free (data);
  if (image) image_dispose (&image);
  if (bitmap) bitmap_dispose (&bitmap);
  return status;
}

int main (int argc, char **argv) {
  int status = 0; /* exit status */
  void *in_obj = NULL;
  object_type_t in_objtype = OT_NONE;

  status = parse_args (argc, argv);
  switch (status) {
    case 0: break;
    case 1:
      message ("No parameters. " HELP_HINT);
      return 0;
    default:
      return -1;
  }

  if (opt_help) {
    show_usage ();
    return 0;
  }

  if (!opt_fi) {
    error ("No %s specified", "input filename");
    return -1;
  }
  if (!opt_fo) {
    error ("No %s specified", "output filename");
    return -1;
  }

  /* from here: on error - set "status" and go to _exit */

  if (opt_ti == opt_to) {
    if ((opt_ti < 0) && (opt_to < 0)) {
      switch (load_file_try (&in_obj, &in_objtype, opt_fi, opt_w, opt_h)) {
        case 0:
          switch (in_objtype) {
            case OT_IMAGE:
              opt_ti = get_image_target (/*(struct image_t *) in_obj*/);
              break;
            case OT_BITMAP:
              opt_ti = get_bitmap_target ((struct bitmap_t *) in_obj);
              break;
            default:
              error ("Internal failure (%s)", "Bad object type");
              status = -1;
              goto _exit;
          }
          break;
        case 1:
          error ("Failed to determine input file type: specify at least "
            "one of input or output file formats");
          status = -1;
          goto _exit;
        default:
          status = -1;
          goto _exit;
      }
    } else if (opt_ti == TT_IMAGE) {
      error ("Input and output file types are the same");
      status = -1;
      goto _exit;
    }
  }

  if (opt_ti < 0) {
    if (set_default_conversion (&opt_ti, opt_to, "output file type")) {
      status = -1;
      goto _exit;
    }
  } else if (opt_to < 0) {
    if (set_default_conversion (&opt_to, opt_ti, "input file type")) {
      status = -1;
      goto _exit;
    }
  }

  if (get_target_object_type (opt_ti) == OT_IMAGE) {

    if (get_target_object_type (opt_to) == OT_BITMAP) {
      bitmap_setup_flags_t sf;
      bitmap_flags_t flags;

      if (in_obj) {
        sf = BMSF_ATTRS;
        flags = BMF_ATTRS;
      } else {
        sf = BMSF_ADR;
        flags = get_target_bitmap_address_mode (opt_to);
      }

      status = convert_image_to_bitmap (
        opt_fo,
        opt_foa,
        sf, flags,
        /*opt_w, opt_h,*/
        opt_fi,
        (struct image_t *) in_obj,
        opt_fia);
    }

  } else if (get_target_object_type (opt_ti) == OT_BITMAP) {

    if (get_target_object_type (opt_to) == OT_IMAGE)
      status = convert_bitmap_to_image (
        opt_fo,
        opt_foa,
        opt_fi,
        BMSF_ADR, get_target_bitmap_address_mode (opt_ti),
        opt_w, opt_h,
        (struct bitmap_t *) in_obj,
        opt_fia);

    if (get_target_object_type (opt_to) == OT_BITMAP)
      status = convert_bitmaps (
        opt_fo,
        opt_foa,
        BMSF_ADR, get_target_bitmap_address_mode (opt_to),
        opt_w, opt_h,
        opt_fi,
        BMSF_ADR, get_target_bitmap_address_mode (opt_ti),
        opt_w, opt_h,
        (struct bitmap_t *) in_obj,
        opt_fia);

  } else {
    error ("Internal failure (%s)", "Bad input file type");
    status = -1;
  }

_exit:
  if (in_obj)
    switch (in_objtype) {
      case OT_IMAGE:
        image_dispose ((struct image_t **) &in_obj);
        break;
      case OT_BITMAP:
        bitmap_dispose ((struct bitmap_t **) &in_obj);
        break;
      default:
        break;
    }
  return status;
}
